# OutfitAI

Gen AI based outfit recommendation system.

## Instructions

1. Create a virtual environment called `.env`, as follows:

```bash
python -m venv .env
```

2. Activate the virtual environment:

*Windows*
```powershell
.\.env\Scripts\Activate.ps1
```

*Ubuntu*
```bash
source .env/bin/activate
```

3. Install all the required dependencies.

```bash
pip install -r requirements.txt
```

4. Running the project.

```bash
python manage.py runserver
```